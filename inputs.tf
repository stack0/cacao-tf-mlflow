variable "username" {
  type = string
  description = "username"
}

variable "project" {
  type = string
  description = "project name"
}

variable "region" {
  type = string
  description = "string, openstack region name; default = IU"
  default = "IU"
}

variable "network" {
  type = string
  description = "network to use for vms"
  default = "auto_allocated_network"
}

variable "instance_name" {
  type = string
  description = "name of jupyterhub instance"
}

variable "instance_count" {
  type = number
  description = "number of instances to launch"
  default = 1
}

variable "image" {
  type = string
  description = "string, image id; image will have priority if both image and image name are provided"
  default = ""
}

variable "image_name" {
  type = string
  description = "string, name of image; image will have priority if both image and image name are provided"
  default = "Featured-Ubuntu22"
}


variable "flavor" {
  type = string
  description = "flavor or size for the worker instances to launch"
  default = "m1.medium"
}

variable "keypair" {
  type = string
  description = "keypair to use when launching"
  default = ""
}

variable "power_state" {
  type = string
  description = "power state of instance"
  default = "active"
}

variable "user_data" {
  type = string
  description = "cloud init script"
  default = ""
}

variable "security_groups" {
  type = list(string)
  description = "array of security group names, either as a a comma-separated string or a list(string). The default is ['default', 'cacao-default']. See local.security_groups"
  default = ["default", "cacao-default"]
}

variable "master_floating_ip" {
  type = string
  description = "floating ip to assign, if one was pre-created; otherwise terraform will auto create one"
  default = ""
}

variable "master_hostname" {
  type = string
  description = "public facing hostname, if set, will be used for the callback url; default is not use set one, which will then use the floating ip"
  default = ""
}

variable "gpu_enable" {
  type = bool
  description = "boolean, whether to enable gpu components"
  default = false
}

variable "gpu_timeslice_enable" {
  type = bool
  description = "boolean, whether to enable gpu timeslicing (only used if gpu_enable == true)"
  default = false
}

variable "gpu_timeslice_num" {
  type = number
  description = "number, number of time slices if gpu timeslicing is enabled; 0 is default, which means auto-slice (js2 only)"
  default = 0
}

variable "do_ansible_execution" {
  type = bool
  description = "boolean, whether to execute ansible"
  default = true
}

variable "ansible_execution_dir" {
  type = string
  description = "string, directory to execute ansible, including location to create the inventory file, where the requirements file is, etc"
  default = "./ansible"
}

variable "k3s_traefik_disable" {
  type = bool
  description = "bool, if true will disable traefik"
  default = false
}

variable "fip_associate_timewait" {
  type = string
  description = "number, time to wait before associating a floating ip in seconds; needed for jetstream; will not be exposed to downstream clients"
  default = "30s"
}

variable "root_storage_source" {
  type = string
  description = "string, source currently supported is image; future values will include volume, snapshot, blank"
  default = "image"
}

variable "root_storage_type" {
  type = string
  description = "string, type is either local or volume"
  default = "local"
}

variable "root_storage_size" {
  type = number
  description = "number, size in GB"
  default = -1
}

variable "root_storage_delete_on_termination" {
  type = bool
  description = "bool, if true delete on termination"
  default = true
}

# this section is for mlflow specific variables
variable "cyverse_user" {
  type = string
  description = "string, irods username"
  default = ""
}

variable "cyverse_pass_base64" {
    type = string
    description = "string, irods password, base64 encoded"
    sensitive = true
    default = ""
}

variable "irods_csi_host" {
    type = string
    description = "string, host for irods csi driver"
    default = "data.cyverse.org"
}

variable "irods_csi_driver_type" {
    type = string
    description = "string, either webdav or irodsfuse"
    default = "irodsfuse"
}

variable "cyverse_host_mount_dir" {
  type = string
  description = "string, the directory to mount the cyverse data store"
  default = "/cyverse"
}

variable "cyverse_remote_base_path" {
  type = string
  description = "string, the directory where to mount from the data store; if empty string, the default will be /iplant/home/<cyverse_user>"
  default = ""
}

variable "cyverse_remote_sqlite_dir" {
  type = string
  description = "string, the relative directory from cyverse_remote_path_base to store the ml sqlite database; if empty, the the default will be 'mlruns'"
  default = "mlruns"
}

variable "cyverse_remote_sqlite_dbname" {
  type = string
  description = "string, the name of the ml sqlite database; if empty, the the default will be 'myflow.db'"
  default = "myflow.db"
}

variable "cyverse_remote_artifact_dir" {
  type = string
  description = "string, the relative directory to store the ml artifacts; if empty, the the default will be 'mlruns/artifacts'"
  default = "mlruns/artifacts"
}

variable "cyverse_artifact_store_enable" {
  type = bool
  description = "string, if true will enable the cyverse artifact store, otherwise local disk will be used"
  default = true
}

variable "mlflow_database_type" {
  type = string
  description = "string, currently only 'cyverse' is supported"
  default = "cyverse"
}

variable "mlflow_version" {
  type = string
  description = "string, mlflow version to use"
  default = "v2.7.0"
}

variable "mlflow_ui_password_base64" {
  type = string
  description = "string, mlflow ui password; if not set, then one will be generated"
  default = ""
}
