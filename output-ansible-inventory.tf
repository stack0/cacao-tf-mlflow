resource "local_file" "ansible-inventory" {
    count = var.do_ansible_execution && var.power_state == "active" ? 1 : 0

    content = templatefile("${path.module}/hosts.yml.tmpl",
    {
        master_ip = var.master_floating_ip == "" ? openstack_compute_floatingip_associate_v2.os_master_floatingip_associate.floating_ip : var.master_floating_ip
        master_name = openstack_compute_instance_v2.os_master_instance.0.name
        worker_ips = openstack_compute_floatingip_associate_v2.os_worker_floatingips_associate.*.floating_ip
        worker_names = openstack_compute_instance_v2.os_worker_instance.*.name # we could use this instead of an generically generated index name
        k3s_hostname = var.master_hostname == "" ? openstack_compute_floatingip_associate_v2.os_master_floatingip_associate.floating_ip : var.master_hostname
        gpu_enable = var.gpu_enable
        gpu_timeslice_enable = local.gpu_timeslice_enable
        gpu_timeslice_num = local.gpu_timeslice_num
        traefik_disable = var.k3s_traefik_disable
        cacao_user = local.real_username
        cyverse_user = var.cyverse_user
        cyverse_pass = base64decode(var.cyverse_pass_base64)
        cyverse_host_mount_dir = var.cyverse_host_mount_dir
        irods_csi_host = var.irods_csi_host
        irods_csi_driver_type = var.irods_csi_driver_type
        cyverse_artifact_store_enable = var.cyverse_artifact_store_enable
        cyverse_remote_base_path = var.cyverse_remote_base_path == "" ? "/iplant/home/${var.cyverse_user}" : var.cyverse_remote_base_path
        cyverse_remote_sqlite_dir = var.cyverse_remote_sqlite_dir
        cyverse_remote_sqlite_dbname = var.cyverse_remote_sqlite_dbname
        cyverse_remote_artifact_dir = var.cyverse_remote_artifact_dir
        mlflow_database_type = var.mlflow_database_type
        mlflow_version = var.mlflow_version
        mlflow_ui_password = var.mlflow_ui_password_base64 == "" ? random_password.password.result : base64decode(var.mlflow_ui_password_base64)
    })
    filename = "${path.module}/ansible/hosts.yml"
}

resource "null_resource" "ansible-execution" {
    count = var.do_ansible_execution && var.power_state == "active" ? 1 : 0

    triggers = {
        always_run = "${timestamp()}"
    }

    provisioner "local-exec" {
        command = "ansible-galaxy install -r requirements.yaml -f"
        working_dir = "${path.module}/ansible"
    }

    provisioner "local-exec" {
        command = "ANSIBLE_HOST_KEY_CHECKING=False ANSIBLE_SSH_PIPELINING=True ANSIBLE_CONFIG=ansible.cfg ansible-playbook -i hosts.yml --forks=10 playbook.yaml"
        working_dir = "${path.module}/ansible"
    }

    depends_on = [
        local_file.ansible-inventory
    ]
}
